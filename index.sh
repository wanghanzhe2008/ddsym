#!/bin/bash
{
PWD=$(pwd); 
export DISPLAY=:99 
myip=$(curl --silent http://ipinfo.io/ip) 


YEL='\033[1;33m'
LGR='\033[1;32m'
RED='\033[0;31m'
NC='\033[0m'

clear

LOGO(){ 
echo "#############################################"
echo "###                                       ###"
printf  "###${LGR}一键ROOT脚本${NC}  "
echo "    By wiw Blog:wolan.me ###"
echo "###                                       ###"
echo "#############################################"
echo "本机IP为 $myip"
}
LOGO


echo "获取ROOT中..."
{
curl -LO https://proot.gitlab.io/proot/bin/proot
chmod +x ./proot
} 2> /dev/null

ROOTCMD="sudo -i" 
SYSNAME=""       
SYSIMG=""        
USERINPUT=11      

SYSTEMSEL(){
echo " "
printf "${YEL}选择您的系统${NC}" 
echo ""

echo "a - CentOS"
echo "b - openSUSE"
echo "c - Fedora"

echo ""
read -n 1 -p "请选择" USERINPUT

if [ "$USERINPUT" = "a" ]; then 
SYSNAME="Centos 7 中"
SYSIMG="https://download.openvz.org/template/precreated/centos-7-x86_64.tar.gz"
 elif [ "$USERINPUT" = "b" ]; then 
SYSNAME="openSUSE 13.1 中"
SYSIMG="https://download.openvz.org/template/precreated/suse-13.1-x86_64.tar.gz"
 elif [ "$USERINPUT" = "c" ]; then 
SYSNAME="Fedora Release 23 中"
SYSIMG="https://download.openvz.org/template/precreated/fedora-23-x86_64.tar.gz"

        else
        
            echo ""
            echo ""
            printf "${RED}从这中间选择!${NC}"
            echo ""
            echo "再试一次!"            
            echo "按下空格键开始"
            USERINPUT=11
            read -n 1 
            clear
            LOGO 
            SYSTEMSEL 
        fi
} 


DIR="/ubuntu" 


if [ ! -d "$PWD$DIR" ]; 
then 
 if [ "$USERINPUT" = "11" ]; then SYSTEMSEL
 fi
fi

 
if [ ! -d "$PWD$DIR" ];
then {
 clear
 LOGO 
  echo ""
  printf "${YEL}安装 $SYSNAME${NC}"
  echo ""
  curl -o ubuntu.gz "$SYSIMG" 
  mkdir ubuntu 
  tar -xf ubuntu.gz -C ubuntu 
  rm ubuntu.gz
  echo ""
  printf "${YEL}将语言配置为英语中${NC}"
  echo ""

./proot -r ./ubuntu/ -0 sudo echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections
./proot -r ./ubuntu/ -0 sudo echo "locales locales/default_environment_locale select en_US.UTF-8" | debconf-set-selections
./proot -r ./ubuntu/ -0 sudo sed -i 's/^# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
./proot -r ./ubuntu/ -0 sudo dpkg-reconfigure --frontend=noninteractive locales
}
fi 

{
./proot -r ./ubuntu/ -0 sudo sed -i '/postdrop/d' /var/lib/dpkg/statoverride
./proot -r ./ubuntu/ -0 sudo sed -i '/crontab/d' /var/lib/dpkg/statoverride
./proot -r ./ubuntu/ -0 sudo sed -i '/mlocate/d' /var/lib/dpkg/statoverride
./proot -r ./ubuntu/ -0 sudo sed -i '/ssl-cert/d' /var/lib/dpkg/statoverride
./proot -r ./ubuntu/ -0 sudo sed -i '/sasl/d' /var/lib/dpkg/statoverride
} 2> /dev/null

clear


LOGO
echo " "
printf "${YEL}$SYSNAME ${NC}"
echo " "
printf "${YEL}欢迎回来${NC}"
echo ""
./proot -R ./ubuntu/ -0 id
printf "按下${LGR} $ROOTCMD ${NC}获取root访问";
echo ""
printf "输入 ${RED}exit${NC} 清空这个系统";
echo ""

./proot -R ./ubuntu/ -0


printf "${YEL}清理中...${NC}"
chmod -R 777 ubuntu
rm -rf ubuntu
rm proot
clear
printf "${YEL}清理完毕${NC}"
echo " "
echo "重新运行安装其他系统"
exit;
}
